package pl.kriminetskiy.govoroon3.models;

/**
 * Created by user on 07.11.17.
 */

public class User {
    public String uid;
    public String email;
    public Integer intId;

    public User(String uid, String email, Integer intId) {
        this.uid = uid;
        this.email = email;
        this.intId = intId;
    }


    public User() {

    }

}
