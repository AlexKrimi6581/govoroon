package pl.kriminetskiy.govoroon3.models;

import pl.kriminetskiy.govoroon3.eventb_prelude.INT;

/**
 * Created by user on 07.11.17.
 */

public class ChatItem {
    private String recipientId;



    private String recipientEmail;
    private Boolean toRead;
    private Boolean Muted;
    private Integer intId1;
    private Integer intId2;



    private Boolean active;

    public ChatItem() {
    }

    public Integer getIntId1() {
        return intId1;
    }

    public void setIntId1(Integer intId1) {
        this.intId1 = intId1;
    }

    public Integer getIntId2() {
        return intId2;
    }

    public void setIntId2(Integer intId2) {
        this.intId2 = intId2;
    }

    public ChatItem(String recipientId, String recipientEmail, Boolean toRead, Boolean muted, Integer intId1, Integer intId2) {
        this.recipientId = recipientId;
        this.recipientEmail = recipientEmail;
        this.toRead = toRead;
        Muted = muted;

        this.intId1 = intId1;
        this.intId2 = intId2;
    }

    public ChatItem(String recipientId, String recipientEmail, Boolean toRead, Boolean muted, Integer intId1, Integer intId2, Boolean active) {
        this.recipientId = recipientId;
        this.recipientEmail = recipientEmail;
        this.toRead = toRead;
        Muted = muted;

        this.intId1 = intId1;
        this.intId2 = intId2;
        this.active = active;
    }

    public String getRecipientId() {
        return recipientId;
    }


    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public Boolean getToRead() {
        return toRead;
    }

    public void setToRead(Boolean toRead) {
        this.toRead = toRead;
    }

    public Boolean getMuted() {
        return Muted;
    }

    public void setMuted(Boolean muted) {
        Muted = muted;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
