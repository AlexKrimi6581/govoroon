package pl.kriminetskiy.govoroon3;

/**
 * Created by Alexandr Kriminetskiy on 31.10.2017.
 */


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import pl.kriminetskiy.govoroon3.adapters.ChatListAdapter;
import pl.kriminetskiy.govoroon3.dialogs.AddChatDialogFragment;
import pl.kriminetskiy.govoroon3.dialogs.BroadcastDialogFragment;
import pl.kriminetskiy.govoroon3.govoroon_sequential.Test_machine3;
import pl.kriminetskiy.govoroon3.govoroon_sequential.delete_chat_session;
import pl.kriminetskiy.govoroon3.govoroon_sequential.mute_chat;
import pl.kriminetskiy.govoroon3.govoroon_sequential.select_chat;
import pl.kriminetskiy.govoroon3.govoroon_sequential.unmute_chat;
import pl.kriminetskiy.govoroon3.models.ChatContent;
import pl.kriminetskiy.govoroon3.models.ChatItem;
import pl.kriminetskiy.govoroon3.models.User;

public class ChatListActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ChatListActivity";
    private FirebaseListAdapter<ChatItem> mAdapter;
    private RelativeLayout activity_chatlist;

    private FloatingActionButton mAddChatSessionButton;
    private DatabaseReference mDatabase;
    private ListView mListOfChats;

    private AdapterView.OnItemClickListener click1;
    private AdapterView.OnItemClickListener click2;
    private AdapterView.OnItemLongClickListener longClick1;
    private AdapterView.OnItemLongClickListener longClick2;

    private android.support.v7.widget.Toolbar mToolbar;
    private ImageButton mSelectAll;
    private ImageButton mMuteButton;
    private ImageButton mUnmuteButton;
    private ImageButton mDeleteButton;
    private ImageButton mBroadcast;
    private FirebaseUser mCurrentUser;
    //private int selectedAfterLongClickPosition = -1;

    //protected ArrayList<Long> itemsSelected = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu,menu);
        return true;
    }
/*
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (selectionMode){
            selectionMode=false;
            getMenuInflater().inflate(R.menu.context_menu, menu);
        }
        else{
            selectionMode=true;
            getMenuInflater().inflate(R.menu.home_menu,menu);
        }

        return super.onPrepareOptionsMenu(menu);
    }
*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        setTitle("GOVOROON");
        actionBar.setSubtitle("all your chat sessions");
        //actionBar.setIcon(R.mipmap.ic_launcher);


        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAddChatSessionButton = (FloatingActionButton) findViewById(R.id.add_chat_session);
        mAddChatSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_chat_session();
            }
        });
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mSelectAll = (ImageButton) findViewById(R.id.select_all);

        mMuteButton = (ImageButton) mToolbar.findViewById(R.id.mute);
        mUnmuteButton = (ImageButton) mToolbar.findViewById(R.id.unmute);
        mDeleteButton = (ImageButton) mToolbar.findViewById(R.id.delete_chat_session);
        mBroadcast = (ImageButton) mToolbar.findViewById(R.id.broadcast);

        mMuteButton.setOnClickListener(this);
        mUnmuteButton.setOnClickListener(this);
        mDeleteButton.setOnClickListener(this);
        mBroadcast.setOnClickListener(this);


        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mAdapter = new ChatListAdapter(this,
                ChatItem.class,
                R.layout.chat_list_item,
                mDatabase.child(GovoroonApp.CHATS_STR).child(mCurrentUser.getUid()));
        mListOfChats = (ListView)findViewById(R.id.all_available_chats);
        mListOfChats.setAdapter(mAdapter);

        mListOfChats.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

        click1 = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {


                String user1Id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                final String username = mAdapter.getItem(position).getRecipientEmail();

                /////~
                mDatabase.child(GovoroonApp.CHATS_STR).child(user1Id).child(mAdapter.getItem(position).getRecipientId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        select_chat sc = new select_chat(((GovoroonApp) getApplication()).getmMachine());

                        if (sc.guard_select_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2())) {
                                sc.run_select_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2());
                                mDatabase.child("active").child(mCurrentUser.getUid()).child(mAdapter.getItem(position).getRecipientId()).setValue(true);
                                    ChatItem ci = new ChatItem(
                                            dataSnapshot.getKey(),
                                    username,
                                    false,
                                    false,
                                            dataSnapshot.getValue(ChatItem.class).getIntId1(),
                                            dataSnapshot.getValue(ChatItem.class).getIntId2(),
                                    true);
                                mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(ci);
                                mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").setValue(true);
                                //mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").getKey();
                                mDatabase.child("active").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(true);

                                Intent intent = new Intent(ChatListActivity.this, ChattingActivity.class);
                                intent.putExtra("recipientId", mAdapter.getItem(position).getRecipientId());
                                intent.putExtra("recipientEmail", mAdapter.getItem(position).getRecipientEmail());
                                //intent.putExtra("muted", mAdapter.getItem(position).getMuted());
                                startActivity(intent);
                        }
                        //final User user1 = dataSnapshot.getValue(User.class);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                ///~)


//
//                Intent intent = new Intent(ChatListActivity.this, ChattingActivity.class);
//                intent.putExtra("recipientId", mAdapter.getItem(position).getRecipientId());
//                intent.putExtra("recipientEmail", mAdapter.getItem(position).getRecipientEmail());
//                //intent.putExtra("muted", mAdapter.getItem(position).getMuted());
//                startActivity(intent);
                //! select_chat

            }
        };

        click2 = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //if (selectedAfterLongClickPosition > -1) listOfChats.setItemChecked(selectedAfterLongClickPosition, true);
                if (mListOfChats.isItemChecked(position)) {
                    view.setBackgroundResource(R.color.amber_200);
                    //listOfChats.setItemChecked(position, true);
                } else {
                    view.setBackgroundResource(android.R.color.transparent);
                    //   listOfChats.setItemChecked(position, false);

                }

                //Snackbar.make(findViewById(R.id.activity_chatlist), ""+listOfChats.getCheckedItemCount()+"  "+checked.size(), Snackbar.LENGTH_SHORT).show();
                if (mListOfChats.getCheckedItemCount() < 1) {

                    quitBottomBar();
                }
                deselectStatus();
            }
        };

        longClick1 = new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mListOfChats.setItemChecked(position, true);

                mListOfChats.setOnItemClickListener(click2);
                mListOfChats.setOnItemLongClickListener(longClick2);

                if (mListOfChats.isItemChecked(position)) {
                    view.setBackgroundResource(R.color.amber_200);
                } else {
                    view.setBackgroundResource(android.R.color.transparent);
                }
                bottomToolbarControl(true);
                deselectStatus();
                return true;
            }

        };

        longClick2 = new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                //NOTE: decide what should be here

                return false;
            }
        };

        mListOfChats.setOnItemClickListener(click1);

        mListOfChats.setOnItemLongClickListener(longClick1);

        mSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mAdapter.getCount() == mListOfChats.getCheckedItemCount()) {
                    for (int i = 0; i < mAdapter.getCount(); ++i) {
                        mListOfChats.setItemChecked(i, false);
                        mListOfChats.getChildAt(i).setBackgroundResource(android.R.color.transparent);
                        quitBottomBar();
                    }
                } else {
                    mSelectAll.setImageResource(R.drawable.ic_unselect_all);
                    for (int i = 0; i < mAdapter.getCount(); ++i) {
                        mListOfChats.setItemChecked(i, true);
                        mListOfChats.getChildAt(i).setBackgroundResource(R.color.amber_200);
                    }
                }
            }
        });
    }


    private void add_chat_session() {
        DialogFragment dialog = new AddChatDialogFragment();
        dialog.show(getSupportFragmentManager(), "AddChatDialogFragment");

    }

    public void bottomToolbarControl(boolean isModeSelection) {
        if(isModeSelection){
            this.mAddChatSessionButton.setVisibility(View.GONE);
            this.mToolbar.setVisibility(View.VISIBLE);
        }
        else{
            this.mAddChatSessionButton.setVisibility(View.VISIBLE);
            this.mToolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(final View v) {
        SparseBooleanArray checked = mListOfChats.getCheckedItemPositions();

        if (v == mMuteButton || v == mUnmuteButton) {
            Log.d(TAG, "mMuteButton or mUnmuteButton");
            for (int i = 0; i < checked.size(); i++) {
                final int key = checked.keyAt(i);
                final boolean value = checked.get(key);
                if (value) {
                    String user1Id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    final String username = mAdapter.getItem(i).getRecipientEmail();

                    mDatabase.child(GovoroonApp.CHATS_STR).child(user1Id).child(mAdapter.getItem(key).getRecipientId()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //if (v == mMuteButton) {
                                mute_chat mc = new mute_chat(((GovoroonApp) getApplication()).getmMachine());
                            //}
                            //else {
                                unmute_chat uc = new unmute_chat(((GovoroonApp) getApplication()).getmMachine());
                            //}
                            if (v == mMuteButton && mc.guard_mute_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2())){
                                mc.run_mute_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2());
                                mDatabase.child("muted").child(mCurrentUser.getUid()).child(mAdapter.getItem(key).getRecipientId()).setValue(v == mMuteButton);
                                ChatItem ci = mAdapter.getItem(key);
                                ci.setMuted(v == mMuteButton);
                                mDatabase.child("muted")
                                .child(mCurrentUser.getUid())
                                .child(mAdapter.getItem(key).getRecipientId()).setValue(v == mMuteButton);
                                mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(ci);
                                mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("muted").setValue(v == mMuteButton);
                                //mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").getKey();
                                mDatabase.child("muted").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(v==mMuteButton);
                            }
                            if (v != mMuteButton && uc.guard_unmute_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2())) {
                                uc.run_unmute_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2());
                                mDatabase.child("muted").child(mCurrentUser.getUid()).child(mAdapter.getItem(key).getRecipientId()).setValue(v == mMuteButton);
                                ChatItem ci = mAdapter.getItem(key);
                                ci.setMuted(v == mMuteButton);
                                mDatabase.child("muted")
                                        .child(mCurrentUser.getUid())
                                        .child(mAdapter.getItem(key).getRecipientId()).setValue(v == mMuteButton);
                                mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(ci);
                                mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("muted").setValue(v == mMuteButton);
                                //mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").getKey();
                                mDatabase.child("muted").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(v==mMuteButton);
                            }

                            //ChatItem ci = mAdapter.getItem(key);
                            //ci.setMuted(value);
                            //mDatabase.child("muted")
                                    //.child(mCurrentUser.getUid())
                                    //.child(mAdapter.getItem(key).getRecipientId()).setValue(v == mMuteButton);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });




                }
            }

        }
        else if (v == mDeleteButton) {
            Log.d(TAG, "mDeleteButton");
            for (int i = 0; i < checked.size(); i++) {
                int key = checked.keyAt(i);
                boolean value = checked.get(key);
                if (value) {
                    ChatItem ci = mAdapter.getItem(key);
                    Integer intId1 = ci.getIntId1();
                    Integer intId2 = ci.getIntId2();

                    Test_machine3.perform_delete_chat_session(
                            ((GovoroonApp)getApplication()).getmMachine(),
                            intId1,
                            intId2
                    );

                    mAdapter.getRef(key).removeValue();
                    mDatabase.child("messages").child(mCurrentUser.getUid()).child(ci.getRecipientId()).removeValue();
                    mListOfChats.setItemChecked(key, false); //
                }
            }
            deselectStatus();
            quitBottomBar();
            unselectAll();

        }
        else if (v == mBroadcast) {
            Log.d(TAG, "mBroadcast");
            DialogFragment dialog = new BroadcastDialogFragment();
            Bundle args = new Bundle();
            //args.put
            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), "BroadcastDialogFragment");

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_sign_out:
                Intent LogIntent = new Intent(this, LoginActivity.class);
                startActivity(LogIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void quitBottomBar(){
        mListOfChats.setOnItemLongClickListener(longClick1);
        mListOfChats.setOnItemClickListener(click1);
        bottomToolbarControl(false);
    }

    public void deselectStatus(){
        if (mAdapter.getCount() == mListOfChats.getCheckedItemCount()){
                               mSelectAll.setImageResource(R.drawable.ic_unselect_all);
                           }
                       else{
                               mSelectAll.setImageResource(R.drawable.ic_select_all);
        }
    }

    public void broadcastChatMessage(String messageText) {

        //////////////////////////////////////////////////////////

//        if (!TextUtils.isEmpty(messageText)) {
//            SparseBooleanArray checked = mListOfChats.getCheckedItemPositions();
//            for (int i = 0; i < checked.size(); i++) {
//                int key = checked.keyAt(i);
//                boolean value = checked.get(key);
//                if (value) {
//                    ChatItem ci = mAdapter.getItem(key);
//
//
//
//                    ChatContent cc = new ChatContent(mCurrentUser.getUid(), ci.getRecipientId(),
//                            mCurrentUser.getEmail(), ci.getRecipientEmail(), messageText, System.currentTimeMillis());
//                    mDatabase.child("messages").child(mCurrentUser.getUid()).child(ci.getRecipientId()).push().setValue(cc);
//                    mDatabase.child("messages").child(ci.getRecipientId()).child(mCurrentUser.getUid()).push().setValue(cc);
//                    Map<String, Object> update = new HashMap<>();
//                    update.put("toRead", true);
//                    mDatabase.child(GovoroonApp.CHATS_STR)
//                            .child(ci.getRecipientId())
//                            .child(mCurrentUser.getUid())
//                            .updateChildren(update);
//                }
//            }
//        }

        if (!TextUtils.isEmpty(messageText)) {
            SparseBooleanArray checked = mListOfChats.getCheckedItemPositions();
            for (int i = 0; i < checked.size(); i++) {
                int key = checked.keyAt(i);
                boolean value = checked.get(key);
                if (value) {
                    final ChatItem ci = mAdapter.getItem(key);
                    ChatContent cc = new ChatContent(mCurrentUser.getUid(), ci.getRecipientId(),
                            mCurrentUser.getEmail(), ci.getRecipientEmail(), messageText, System.currentTimeMillis());
                    mDatabase.child("messages").child(mCurrentUser.getUid()).child(ci.getRecipientId()).push().setValue(cc);
                    mDatabase.child("messages").child(ci.getRecipientId()).child(mCurrentUser.getUid()).push().setValue(cc);


                    mDatabase.child(GovoroonApp.CHATS_STR)
                            .child(ci.getRecipientId())
                            .child(mCurrentUser.getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getValue() == null) {

                                        mDatabase.child(GovoroonApp.USERS_STR)
                                                .child(mCurrentUser.getUid())
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(final DataSnapshot dataSnapshot1) {
                                                        mDatabase.child(GovoroonApp.USERS_STR)
                                                                .child(ci.getRecipientId())
                                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(final DataSnapshot dataSnapshot2) {
                                                                        final Integer intId1 = dataSnapshot1.getValue(User.class).intId;
                                                                        final Integer intId2 = dataSnapshot2.getValue(User.class).intId;
                                                                        ChatItem new_ci = new ChatItem(mCurrentUser.getUid(), mCurrentUser.getEmail(), true, false, intId1, intId2);
                                                                        mDatabase.child(GovoroonApp.CHATS_STR)
                                                                                .child(ci.getRecipientId())
                                                                                .child(mCurrentUser.getUid())
                                                                                .setValue(new_ci);
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {

                                                                    }
                                                                });
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                    } else {
                                        Map<String, Object> update = new HashMap<>();
                                        update.put("toRead", true);
                                        mDatabase.child(GovoroonApp.CHATS_STR)
                                                .child(ci.getRecipientId())
                                                .child(mCurrentUser.getUid())
                                                .updateChildren(update);
                                    }
                                    Log.d(TAG, dataSnapshot.toString());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                }
            }
        }

        ///////////////////////////////////

        deselectStatus();
        quitBottomBar();
        unselectAll();
    }

    public void unselectAll(){
        for (int i = 0; i< mAdapter.getCount(); ++i){
            mListOfChats.setItemChecked(i, false);
            mListOfChats.getChildAt(i).setBackgroundResource(android.R.color.transparent);

        }

    }
}


