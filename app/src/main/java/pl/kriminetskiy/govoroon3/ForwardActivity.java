package pl.kriminetskiy.govoroon3;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import pl.kriminetskiy.govoroon3.adapters.ChatListAdapter;
import pl.kriminetskiy.govoroon3.models.ChatContent;
import pl.kriminetskiy.govoroon3.models.ChatItem;
import pl.kriminetskiy.govoroon3.models.User;

public class ForwardActivity extends AppCompatActivity {

    private static final String TAG = "ForwardActivity";
    private FirebaseListAdapter<ChatItem> mAdapter;

    private FloatingActionButton mSendButton;
    private DatabaseReference mDatabase;
    private ListView mListOfChats;
    private String mMessageText;
    private String mSenderId;
    private String mSenderEmail;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_chats);
        mMessageText = getIntent().getExtras().getString("messageText");
        mSenderId = getIntent().getExtras().getString("senderId");
        mSenderEmail = getIntent().getExtras().getString("senderEmail");

        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mSendButton = (FloatingActionButton) findViewById(R.id.send);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessageToChats();
            }
        });

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mAdapter = new ChatListAdapter(this,
                ChatItem.class,
                R.layout.chat_list_item,
                mDatabase.child(GovoroonApp.CHATS_STR).child(user.getUid()));
        mListOfChats = (ListView) findViewById(R.id.all_available_chats);
        mListOfChats.setAdapter(mAdapter);
        mListOfChats.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, String.valueOf(mListOfChats.isItemChecked(position)));
                if (mListOfChats.isItemChecked(position)) {
                    view.setBackgroundResource(R.color.amber_200);
                } else {
                    view.setBackgroundResource(android.R.color.transparent);
                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, ChatListActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_sign_out:
                Intent LogIntent = new Intent(this, LoginActivity.class);
                startActivity(LogIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendMessageToChats() {
        SparseBooleanArray checked = mListOfChats.getCheckedItemPositions();
        for (int i = 0; i < checked.size(); i++) {
            int key = checked.keyAt(i);
            boolean value = checked.get(key);
            if (value) {
                final ChatItem ci = mAdapter.getItem(key);
                ChatContent cc = new ChatContent(mSenderId, ci.getRecipientId(),
                        mSenderEmail, ci.getRecipientEmail(), mMessageText, System.currentTimeMillis());
                mDatabase.child("messages").child(mSenderId).child(ci.getRecipientId()).push().setValue(cc);
                mDatabase.child("messages").child(ci.getRecipientId()).child(mSenderId).push().setValue(cc);


                mDatabase.child(GovoroonApp.CHATS_STR)
                        .child(ci.getRecipientId())
                        .child(mSenderId)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getValue() == null) {

                                    mDatabase.child(GovoroonApp.USERS_STR)
                                            .child(mSenderId)
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(final DataSnapshot dataSnapshot1) {
                                                    mDatabase.child(GovoroonApp.USERS_STR)
                                                            .child(ci.getRecipientId())
                                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(final DataSnapshot dataSnapshot2) {
                                                                    final Integer intId1 = dataSnapshot1.getValue(User.class).intId;
                                                                    final Integer intId2 = dataSnapshot2.getValue(User.class).intId;
                                                                    ChatItem new_ci = new ChatItem(mSenderId, mSenderEmail, true, false, intId1, intId2);
                                                                    mDatabase.child(GovoroonApp.CHATS_STR)
                                                                            .child(ci.getRecipientId())
                                                                            .child(mSenderId)
                                                                            .setValue(new_ci);
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {

                                                                }
                                                            });
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                } else {
                                    Map<String, Object> update = new HashMap<>();
                                    update.put("toRead", true);
                                    mDatabase.child(GovoroonApp.CHATS_STR)
                                            .child(ci.getRecipientId())
                                            .child(mSenderId)
                                            .updateChildren(update);
                                }
                                Log.d(TAG, dataSnapshot.toString());
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


            }
        }
        finish();
    }
}
