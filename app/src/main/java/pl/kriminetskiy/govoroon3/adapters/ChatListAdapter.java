package pl.kriminetskiy.govoroon3.adapters;

import android.app.Activity;
import android.database.DataSetObserver;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import pl.kriminetskiy.govoroon3.ChatListActivity;
import pl.kriminetskiy.govoroon3.R;
import pl.kriminetskiy.govoroon3.models.ChatItem;

/**
 * Created by user on 07.11.17.
 */

public class ChatListAdapter extends FirebaseListAdapter<ChatItem> {
    public ChatListAdapter(Activity activity, Class<ChatItem> modelClass, int modelLayout, Query ref) {
        super(activity, modelClass, modelLayout, ref);
    }

    @Override
    protected void populateView(View v, ChatItem model, int position) {
        TextView t1 = (TextView) v.findViewById(R.id.text_view_user_alphabet);
        TextView t2 = (TextView) v.findViewById(R.id.text_view_username);
        ImageView note = (ImageView) v.findViewById(R.id.notification);

        t1.setText(model.getRecipientEmail().substring(0, 1));
        t2.setText(model.getRecipientEmail());

        // NOTE: If MUTED then use another src for icon
        //DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        if (model.getToRead()) {
            t1.setBackgroundResource(R.drawable.circle_accent);
            note.setVisibility(View.VISIBLE);
        }
        else
        {
            t1.setBackgroundResource(R.drawable.circle_primary);
            note.setVisibility(View.INVISIBLE);
            try {
                if (!model.getMuted() || model.getMuted() == null) {
                    t1.setBackgroundResource(R.drawable.circle_primary);
                    note.setImageResource(R.drawable.notification);
                } else {
                    t1.setBackgroundResource(R.drawable.circle_cold);
                    note.setImageResource(R.drawable.ic_muted_chat);
                    note.setVisibility(View.VISIBLE);
                }
            }
            catch (Exception exc) {}
        }
    }
}
