package pl.kriminetskiy.govoroon3.adapters;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;
import java.util.Map;

import pl.kriminetskiy.govoroon3.GovoroonApp;
import pl.kriminetskiy.govoroon3.R;
import pl.kriminetskiy.govoroon3.models.ChatContent;

/**
 * Created by user on 08.11.17.
 */

public class MessageListAdapter extends FirebaseListAdapter<ChatContent> {
    private final DatabaseReference mDatabase;
    private final String mRecipientId;
    private final String mSenderId;

    public MessageListAdapter(Activity activity, Class<ChatContent> modelClass, int modelLayout, Query ref, String senderId, String recipientId) {
        super(activity, modelClass, modelLayout, ref);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mSenderId = senderId;
        mRecipientId = recipientId;
    }



    @Override
    protected void populateView(View v, ChatContent model, int position) {

        TextView t1 = (TextView) v.findViewById(R.id.text_view_user_alphabet);
        TextView t2 = (TextView) v.findViewById(R.id.text_view_chat_message);

        t1.setText(model.getSenderEmail().substring(0, 1));
        t2.setText(model.getMessage());

        if (position == this.getCount() - 1 ) {
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("toRead", false);

            mDatabase.child(GovoroonApp.CHATS_STR)
                    .child(mSenderId)
                    .child(mRecipientId)
                    .updateChildren(childUpdates);
        }

        //if (false){
        if (FirebaseAuth.getInstance().getCurrentUser().getEmail().equals(model.getSenderEmail())){
        //if (mSenderId == mRecipientId){  /// THIS CONDITION!
        //if (mDatabase.child(GovoroonApp.CHATS_STR).child(mSenderId) == mDatabase.child(GovoroonApp.CHATS_STR).child(mRecipientId)){
            LinearLayout lyo = (LinearLayout) v.findViewById(R.id.layout_message);
            t1.setVisibility(View.GONE);

            t2.setBackgroundResource(R.color.govoroon_two);
            lyo.setGravity(Gravity.RIGHT);

        }
        else{
            LinearLayout lyo = (LinearLayout) v.findViewById(R.id.layout_message);
            t1.setVisibility(View.VISIBLE);

            t2.setBackgroundResource(R.color.govoroon_three);
            lyo.setGravity(Gravity.LEFT);
        }
    }
}