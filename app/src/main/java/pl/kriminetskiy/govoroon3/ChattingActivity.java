package pl.kriminetskiy.govoroon3;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import pl.kriminetskiy.govoroon3.adapters.MessageListAdapter;
import pl.kriminetskiy.govoroon3.dialogs.ForwardDeleteDialogFragment;
import pl.kriminetskiy.govoroon3.govoroon_sequential.unselect_chat;
import pl.kriminetskiy.govoroon3.models.ChatContent;
import pl.kriminetskiy.govoroon3.models.ChatItem;
import pl.kriminetskiy.govoroon3.models.User;

/**
 * Created by user on 08.11.17.
 */

public class ChattingActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final String TAG = "ChattingActivity";
    private MessageListAdapter mAdapter;
    private DatabaseReference mDatabase;
    private ImageButton mSendButton;
    private EditText mInput;
    private FirebaseUser mCurrentUser;
    private String mRecipientId;
    private String mRecipientEmail;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu,menu);
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);


        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        //mActionBar.

        mRecipientId = getIntent().getExtras().getString("recipientId");
        mRecipientEmail = getIntent().getExtras().getString("recipientEmail");

        Log.d(TAG, mRecipientId);
        Log.d(TAG, mRecipientEmail);

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mSendButton = (ImageButton) findViewById(R.id.send);
        mSendButton.setOnClickListener(this);

        mInput = (EditText) findViewById(R.id.input);

        setTitle("Chat: " + mRecipientEmail.split("@")[0]);
        mActionBar.setSubtitle(mRecipientEmail);

        mAdapter = new MessageListAdapter(this,
                ChatContent.class,
                R.layout.message_item_mine,
                mDatabase.child("messages").child(mCurrentUser.getUid()).child(mRecipientId), mCurrentUser.getUid(), mRecipientId);
        ListView listOfMessages = (ListView)findViewById(R.id.chat_messages);
        listOfMessages.setAdapter(mAdapter);
        listOfMessages.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(mInput.getText())) {
            return;
        }

        mDatabase.child("muted")
                .child(mRecipientId)
                .child(mCurrentUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Boolean muted = dataSnapshot.getValue(Boolean.class);
                        ChatContent cc = new ChatContent(mCurrentUser.getUid(), mRecipientId, mCurrentUser.getEmail(), mRecipientEmail, mInput.getText().toString(), System.currentTimeMillis());
                        mDatabase.child("messages").child(mCurrentUser.getUid()).child(mRecipientId).push().setValue(cc);
                        if (muted == null || !muted) {

                            mDatabase.child("messages").child(mRecipientId).child(mCurrentUser.getUid()).push().setValue(cc);

                            mDatabase.child(GovoroonApp.USERS_STR)
                                    .child(mCurrentUser.getUid())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot dataSnapshot1) {
                                            mDatabase.child(GovoroonApp.USERS_STR)
                                                    .child(mRecipientId)
                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(final DataSnapshot dataSnapshot2) {
                                                            final Integer intId1 = dataSnapshot1.getValue(User.class).intId;
                                                            final Integer intId2 = dataSnapshot2.getValue(User.class).intId;
                                                            ChatItem ci = new ChatItem(mRecipientId, mRecipientEmail, true, false, intId1, intId2);
                                                            mDatabase.child(GovoroonApp.CHATS_STR)
                                                                    .child(mRecipientId)
                                                                    .child(mCurrentUser.getUid())
                                                                    .setValue(ci);
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                        mInput.getText().clear();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DialogFragment dialog = new ForwardDeleteDialogFragment();
        Bundle args = new Bundle();
        Log.d(TAG, mAdapter.getRef(position).getKey());
        args.putString("senderId", mCurrentUser.getUid());
        args.putString("senderEmail", mCurrentUser.getEmail());
        args.putString("recipientId", mRecipientId);
        args.putString("messageId", mAdapter.getRef(position).getKey());
        args.putString("messageText",
                ((TextView) view.findViewById(R.id.text_view_chat_message)).getText().toString());
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "ForwardDeleteDialogFragment");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                final Intent intent = new Intent(this, ChatListActivity.class);

                String user1Id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                final String username = mRecipientEmail;

                /////~
                //mDatabase.child(GovoroonApp.CHATS_STR).child(user1Id).child(mAdapter.getItem(position).getRecipientId()).addListenerForSingleValueEvent(new ValueEventListener() {
                mDatabase.child(GovoroonApp.CHATS_STR).child(user1Id).child(mRecipientId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        unselect_chat sc = new unselect_chat(((GovoroonApp) getApplication()).getmMachine());

                        if (sc.guard_unselect_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2())) {
                            sc.run_unselect_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2());
                            mDatabase.child("active").child(mCurrentUser.getUid()).child(mRecipientId).setValue(false);
                            ChatItem ci = new ChatItem(
                                    dataSnapshot.getKey(),
                                    username,
                                    false,
                                    false,
                                    dataSnapshot.getValue(ChatItem.class).getIntId1(),
                                    dataSnapshot.getValue(ChatItem.class).getIntId2(),
                                    false);
                            mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(ci);
                            mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").setValue(false);
                            //mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").getKey();
                            mDatabase.child("active").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(false);


                            startActivity(intent);
                        }
                        //final User user1 = dataSnapshot.getValue(User.class);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                //Intent intent = new Intent(this, ChatListActivity.class);
                //startActivity(intent);

                break;
            case R.id.menu_sign_out:
                Intent LogIntent = new Intent(this, LoginActivity.class);
                startActivity(LogIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        final Intent intent = new Intent(this, ChatListActivity.class);
                String user1Id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                final String username = mRecipientEmail;

                /////~
        mDatabase.child(GovoroonApp.CHATS_STR).child(user1Id).child(mRecipientId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        unselect_chat sc = new unselect_chat(((GovoroonApp) getApplication()).getmMachine());

                        if (sc.guard_unselect_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2())) {
                            sc.run_unselect_chat(dataSnapshot.getValue(ChatItem.class).getIntId1(), dataSnapshot.getValue(ChatItem.class).getIntId2());
                            mDatabase.child("active").child(mCurrentUser.getUid()).child(mRecipientId).setValue(false);
                            ChatItem ci = new ChatItem(
                                    dataSnapshot.getKey(),
                                    username,
                                    false,
                                    false,
                                    dataSnapshot.getValue(ChatItem.class).getIntId1(),
                                    dataSnapshot.getValue(ChatItem.class).getIntId2(),
                                    false);
                            mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(ci);
                            mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").setValue(false);
                            //mDatabase.child(GovoroonApp.CHATS_STR).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).child("active").getKey();
                            mDatabase.child("active").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dataSnapshot.getKey()).setValue(false);


                            startActivity(intent);
                        }
                        //final User user1 = dataSnapshot.getValue(User.class);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                //Intent intent = new Intent(this, ChatListActivity.class);
                //startActivity(intent);

        super.onBackPressed();
    }
}
