package pl.kriminetskiy.govoroon3.govoroon_sequential;

import pl.kriminetskiy.govoroon3.eventb_prelude.*;
import pl.kriminetskiy.govoroon3.Util.*;
//@ model import org.jmlspecs.models.JMLObjectSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class machine3{
	private static final Integer max_integer = Utilities.max_integer;
	private static final Integer min_integer = Utilities.min_integer;

	create_chat_session evt_create_chat_session = new create_chat_session(this);
	broadcast evt_broadcast = new broadcast(this);
	delete_chat_session evt_delete_chat_session = new delete_chat_session(this);
	unmute_chat evt_unmute_chat = new unmute_chat(this);
	select_chat evt_select_chat = new select_chat(this);
	unselect_chat evt_unselect_chat = new unselect_chat(this);
	forward evt_forward = new forward(this);
	delete_content evt_delete_content = new delete_content(this);
	mute_chat evt_mute_chat = new mute_chat(this);
	chatting evt_chatting = new chatting(this);
	remove_content evt_remove_content = new remove_content(this);
	add_user evt_add_user = new add_user(this);


	/******Set definitions******/
	//@ public static constraint CONTENT.equals(\old(CONTENT)); 
	public static final BSet<Integer> CONTENT = new Enumerated(min_integer,max_integer);

	//@ public static constraint USER.equals(\old(USER)); 
	public static final BSet<Integer> USER = new Enumerated(min_integer,max_integer);


	/******Constant definitions******/


	/******Axiom definitions******/
	/*@ public static invariant CONTENT.finite(); */


	/******Variable definitions******/
	/*@ spec_public */ private BRelation<Integer,Integer> active;

	/*@ spec_public */ private BRelation<Integer,Integer> chat;

	/*@ spec_public */ private BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> chatcontent;

	/*@ spec_public */ private BSet<Integer> content;

	/*@ spec_public */ private BRelation<Integer,Integer> contentseq;

	/*@ spec_public */ private Integer contentsize;

	/*@ spec_public */ private BRelation<Integer,Integer> inactive;

	/*@ spec_public */ private BRelation<Integer,Integer> muted;

	/*@ spec_public */ private BRelation<Integer,Integer> owner;

	/*@ spec_public */ private BRelation<Integer,BSet<Integer>> reading_content;

	/*@ spec_public */ private BRelation<Integer,Integer> toread;

	/*@ spec_public */ private BSet<Integer> user;




	/******Invariant definition******/
	/*@ public invariant
		user.isSubset(USER) &&
		content.isSubset(CONTENT) &&
		 chat.domain().isSubset(user) && chat.range().isSubset(user) && BRelation.cross(user,user).has(chat) &&
		 active.domain().isSubset(user) && active.range().isSubset(user) && active.isaFunction() && BRelation.cross(user,user).has(active) &&
		 muted.domain().isSubset(user) && muted.range().isSubset(user) && BRelation.cross(user,user).has(muted) &&
		active.isSubset(chat) &&
		muted.isSubset(chat) &&
		 chatcontent.domain().isSubset(user) && chatcontent.range().isSubset(BRelation.cross(BRelation.cross(user,user),((content).pow()))) && chatcontent.isaFunction() && BRelation.cross(user,BRelation.cross(BRelation.cross(user,user),((content).pow()))).has(chatcontent) &&
		(muted.intersection(active)).equals(BSet.EMPTY) &&
		 (\forall Integer u;  (\forall Integer u1;  (\forall Integer u2;  (\forall Integer c;((chatcontent.domain().has(u) && chatcontent.apply(u).domain().has(new Pair<Integer,Integer>(u1,u2)) && chatcontent.apply(u).apply(new Pair<Integer,Integer>(u1,u2)).has(c)) ==> (u.equals(u1) || u.equals(u2))))))) &&
		 toread.domain().isSubset(user) && toread.range().isSubset(user) && BRelation.cross(user,user).has(toread) &&
		toread.isSubset(chat) &&
		inactive.isSubset(chat) &&
		(active.union(toread.union(inactive.union(muted)))).equals(chat) &&
		(active.intersection(toread)).equals(BSet.EMPTY) &&
		(active.intersection(inactive)).equals(BSet.EMPTY) &&
		 owner.domain().equals(content) && owner.range().isSubset(user) && owner.isaFunction() && BRelation.cross(content,user).has(owner) &&
		owner.range().isSubset(chatcontent.domain()) &&
		 (\forall Integer c;  (\forall Integer u1;  (\forall Integer u2;((content.has(c) && user.has(u1) && user.has(u2) && owner.has(new Pair<Integer,Integer>(c,u1)) && owner.has(new Pair<Integer,Integer>(c,u2))) ==> (u1.equals(u2)))))) &&
		 (\forall Integer c;((content.has(c)) ==> (chatcontent.domain().has(owner.apply(c))))) &&
		 (\forall Integer c;  (\forall Integer u;  (\forall Pair<Integer,Integer> ch;((content.has(c) && user.has(u) && chat.has(ch) && chatcontent.domain().has(u) && chatcontent.apply(u).domain().has(ch) && chatcontent.apply(u).apply(ch).has(c)) ==> (owner.domain().has(c)))))) &&
		(contentsize).compareTo(new Integer(0)) >= 0 &&
		 contentseq.domain().equals(new Enumerated(new Integer(1),contentsize)) && contentseq.range().equals(content) && contentseq.isaFunction() && contentseq.inverse().isaFunction() && BRelation.cross(new Enumerated(new Integer(1),contentsize),content).has(contentseq) &&
		NAT.instance.has(contentsize) &&
		content.isSubset(new Best<Integer>(new JMLObjectSet {Integer i | (\exists Integer e; (new Enumerated(new Integer(1),contentsize).has(i) && contentseq.domain().has(i)); e.equals(contentseq.apply(i)))})) &&
		 reading_content.domain().isSubset(user) && reading_content.range().isSubset(((content).pow())) && reading_content.isaFunction() && BRelation.cross(user,((content).pow())).has(reading_content); */


	/******Getter and Mutator methods definition******/
	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.owner;*/
	public /*@ pure */ BRelation<Integer,Integer> get_owner(){
		return this.owner;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.owner;
	    ensures this.owner == owner;*/
	public void set_owner(BRelation<Integer,Integer> owner){
		this.owner = owner;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.toread;*/
	public /*@ pure */ BRelation<Integer,Integer> get_toread(){
		return this.toread;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.toread;
	    ensures this.toread == toread;*/
	public void set_toread(BRelation<Integer,Integer> toread){
		this.toread = toread;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.contentseq;*/
	public /*@ pure */ BRelation<Integer,Integer> get_contentseq(){
		return this.contentseq;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.contentseq;
	    ensures this.contentseq == contentseq;*/
	public void set_contentseq(BRelation<Integer,Integer> contentseq){
		this.contentseq = contentseq;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.inactive;*/
	public /*@ pure */ BRelation<Integer,Integer> get_inactive(){
		return this.inactive;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.inactive;
	    ensures this.inactive == inactive;*/
	public void set_inactive(BRelation<Integer,Integer> inactive){
		this.inactive = inactive;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.chatcontent;*/
	public /*@ pure */ BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> get_chatcontent(){
		return this.chatcontent;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.chatcontent;
	    ensures this.chatcontent == chatcontent;*/
	public void set_chatcontent(BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> chatcontent){
		this.chatcontent = chatcontent;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.chat;*/
	public /*@ pure */ BRelation<Integer,Integer> get_chat(){
		return this.chat;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.chat;
	    ensures this.chat == chat;*/
	public void set_chat(BRelation<Integer,Integer> chat){
		this.chat = chat;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.contentsize;*/
	public /*@ pure */ Integer get_contentsize(){
		return this.contentsize;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.contentsize;
	    ensures this.contentsize == contentsize;*/
	public void set_contentsize(Integer contentsize){
		this.contentsize = contentsize;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.active;*/
	public /*@ pure */ BRelation<Integer,Integer> get_active(){
		return this.active;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.active;
	    ensures this.active == active;*/
	public void set_active(BRelation<Integer,Integer> active){
		this.active = active;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.muted;*/
	public /*@ pure */ BRelation<Integer,Integer> get_muted(){
		return this.muted;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.muted;
	    ensures this.muted == muted;*/
	public void set_muted(BRelation<Integer,Integer> muted){
		this.muted = muted;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.reading_content;*/
	public /*@ pure */ BRelation<Integer,BSet<Integer>> get_reading_content(){
		return this.reading_content;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.reading_content;
	    ensures this.reading_content == reading_content;*/
	public void set_reading_content(BRelation<Integer,BSet<Integer>> reading_content){
		this.reading_content = reading_content;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.user;*/
	public /*@ pure */ BSet<Integer> get_user(){
		return this.user;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.user;
	    ensures this.user == user;*/
	public void set_user(BSet<Integer> user){
		this.user = user;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.content;*/
	public /*@ pure */ BSet<Integer> get_content(){
		return this.content;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.content;
	    ensures this.content == content;*/
	public void set_content(BSet<Integer> content){
		this.content = content;
	}



	/*@ public normal_behavior
	    requires true;
	    assignable \everything;
	    ensures
		user.isEmpty() &&
		content.isEmpty() &&
		chat.isEmpty() &&
		active.isEmpty() &&
		chatcontent.isEmpty() &&
		muted.isEmpty() &&
		toread.isEmpty() &&
		inactive.isEmpty() &&
		owner.isEmpty() &&
		contentseq.isEmpty() &&
		contentsize == 0 &&
		reading_content.isEmpty();*/
	public machine3(){
		user = new BSet<Integer>();
		content = new BSet<Integer>();
		chat = new BRelation<Integer,Integer>();
		active = new BRelation<Integer,Integer>();
		chatcontent = new BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>();
		muted = new BRelation<Integer,Integer>();
		toread = new BRelation<Integer,Integer>();
		inactive = new BRelation<Integer,Integer>();
		owner = new BRelation<Integer,Integer>();
		contentseq = new BRelation<Integer,Integer>();
		contentsize = 0;
		reading_content = new BRelation<Integer,BSet<Integer>>();

	}
}