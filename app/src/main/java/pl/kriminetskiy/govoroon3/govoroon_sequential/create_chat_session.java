package pl.kriminetskiy.govoroon3.govoroon_sequential;

import pl.kriminetskiy.govoroon3.eventb_prelude.BRelation;
import pl.kriminetskiy.govoroon3.eventb_prelude.BSet;
import pl.kriminetskiy.govoroon3.eventb_prelude.Pair;

public class create_chat_session{
	/*@ spec_public */ private machine3 machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public create_chat_session(machine3 m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_user().has(u1) && machine.get_user().has(u2) && !machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().domain().has(u1)); */
	public /*@ pure */ boolean guard_create_chat_session( Integer u1, Integer u2) {
		return (machine.get_user().has(u1) && machine.get_user().has(u2) && !machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().domain().has(u1));
	}

	/*@ public normal_behavior
		requires guard_create_chat_session(u1,u2);
		assignable machine.chat, machine.chatcontent, machine.inactive;
		ensures guard_create_chat_session(u1,u2) &&  machine.get_chat().equals(\old((machine.get_chat().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2)))))) &&  machine.get_chatcontent().equals(\old((machine.get_chatcontent().override(new BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(new Pair<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(u1,(machine.get_chatcontent().apply(u1).override(new BRelation<Pair<Integer,Integer>,ERROR>(new Pair<Pair<Integer,Integer>,ERROR>(new Pair<Integer,Integer>(u1,u2),BRelation.EMPTY),new Pair<Pair<Integer,Integer>,ERROR>(new Pair<Integer,Integer>(u2,u1),BRelation.EMPTY)))))))))) &&  machine.get_inactive().equals(\old((machine.get_inactive().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2)))))); 
	 also
		requires !guard_create_chat_session(u1,u2);
		assignable \nothing;
		ensures true; */
	public void run_create_chat_session( Integer u1, Integer u2){
		if(guard_create_chat_session(u1,u2)) {
			BRelation<Integer,Integer> chat_tmp = machine.get_chat();
			BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> chatcontent_tmp = machine.get_chatcontent();
			BRelation<Integer,Integer> inactive_tmp = machine.get_inactive();

			machine.set_chat((chat_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2)))));
			machine.set_chatcontent((chatcontent_tmp.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u1, (chatcontent_tmp.apply(u1).override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(new Pair<Pair<Integer, Integer>, BSet<Integer>>(new Pair<Integer, Integer>(u1, u2), BRelation.EMPTY),new Pair<Pair<Integer, Integer>, BSet<Integer>>(new Pair<Integer, Integer>(u2, u1), BRelation.EMPTY)))))))));
			machine.set_inactive((inactive_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2)))));

			System.out.println("create_chat_session executed u1: " + u1 + " u2: " + u2 + " ");
		}
	}

}
