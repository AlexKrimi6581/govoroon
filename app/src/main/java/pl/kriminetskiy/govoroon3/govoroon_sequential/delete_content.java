package pl.kriminetskiy.govoroon3.govoroon_sequential;

import pl.kriminetskiy.govoroon3.eventb_prelude.*;
import pl.kriminetskiy.govoroon3.Util.Utilities;

public class delete_content{
	/*@ spec_public */ private machine3 machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public delete_content(machine3 m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_user().has(u1) && machine.get_user().has(u2) && machine.get_content().has(c) && machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_active().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().domain().has(u2) && machine.get_chatcontent().apply(u2).domain().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().apply(u2).apply(new Pair<Integer,Integer>(u1,u2)).has(c) && !machine.get_owner().apply(c).equals(u1)); */
	public /*@ pure */ boolean guard_delete_content( Integer c, Integer u1, Integer u2) {
		return (machine.get_user().has(u1) && machine.get_user().has(u2) && machine.get_content().has(c) && machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_active().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().domain().has(u2) && machine.get_chatcontent().apply(u2).domain().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().apply(u2).apply(new Pair<Integer,Integer>(u1,u2)).has(c) && !machine.get_owner().apply(c).equals(u1));
	}

	/*@ public normal_behavior
		requires guard_delete_content(c,u1,u2);
		assignable machine.chatcontent;
		ensures guard_delete_content(c,u1,u2) &&  machine.get_chatcontent().equals(\old((machine.get_chatcontent().override(new BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(new Pair<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(u2,(machine.get_chatcontent().apply(u2).override(new BRelation<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Integer,Integer>(u1,u2),machine.get_chatcontent().apply(u2).apply(new Pair<Integer,Integer>(u1,u2)).difference(new BSet<Integer>(c)))))))))))); 
	 also
		requires !guard_delete_content(c,u1,u2);
		assignable \nothing;
		ensures true; */
	public void run_delete_content( Integer c, Integer u1, Integer u2){
		if(guard_delete_content(c,u1,u2)) {
			BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> chatcontent_tmp = machine.get_chatcontent();

			machine.set_chatcontent((chatcontent_tmp.override(new BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(new Pair<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(u2,(chatcontent_tmp.apply(u2).override(new BRelation<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Integer,Integer>(u1,u2),chatcontent_tmp.apply(u2).apply(new Pair<Integer,Integer>(u1,u2)).difference(new BSet<Integer>(c)))))))))));

			System.out.println("delete_content executed c: " + c + " u1: " + u1 + " u2: " + u2 + " ");
		}
	}

}
