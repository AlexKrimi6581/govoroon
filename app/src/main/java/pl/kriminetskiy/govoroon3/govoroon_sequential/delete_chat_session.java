package pl.kriminetskiy.govoroon3.govoroon_sequential;

import pl.kriminetskiy.govoroon3.eventb_prelude.*;
import pl.kriminetskiy.govoroon3.Util.Utilities;

public class delete_chat_session{
	/*@ spec_public */ private machine3 machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public delete_chat_session(machine3 m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_user().has(u1) && machine.get_user().has(u2) && machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().domain().has(u1) && machine.get_chatcontent().apply(u1).domain().has(new Pair<Integer,Integer>(u1,u2))); */
	public /*@ pure */ boolean guard_delete_chat_session( Integer u1, Integer u2) {
		return (machine.get_user().has(u1) && machine.get_user().has(u2) && machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().domain().has(u1) && machine.get_chatcontent().apply(u1).domain().has(new Pair<Integer,Integer>(u1,u2)));
	}

	/*@ public normal_behavior
		requires guard_delete_chat_session(u1,u2);
		assignable machine.muted, machine.chatcontent, machine.chat, machine.active, machine.toread, machine.inactive;
		ensures guard_delete_chat_session(u1,u2) &&  machine.get_muted().equals(\old(machine.get_muted().difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))))) &&  machine.get_chatcontent().equals(\old((machine.get_chatcontent().override(new Best<Integer>(new JMLObjectSet {Integer u | (\exists Integer e; (machine.get_user().has(u) && machine.get_chatcontent().domain().has(u) && machine.get_chatcontent().apply(u).domain().has(new Pair<Integer,Integer>(u1,u2))); e.equals(new Pair<BRelation<Pair<Integer,Integer>,BSet<Integer>>,ERROR>(u,machine.get_chatcontent().apply(u).domainSubtraction(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))))))}))))) &&  machine.get_chat().equals(\old(machine.get_chat().difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))))) &&  machine.get_active().equals(\old(machine.get_active().difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))))) &&  machine.get_toread().equals(\old(machine.get_toread().difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))))) &&  machine.get_inactive().equals(\old(machine.get_inactive().difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))))); 
	 also
		requires !guard_delete_chat_session(u1,u2);
		assignable \nothing;
		ensures true; */
	public void run_delete_chat_session( Integer u1, Integer u2){
		if(guard_delete_chat_session(u1,u2)) {
			BRelation<Integer,Integer> muted_tmp = machine.get_muted();
			BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> chatcontent_tmp = machine.get_chatcontent();
			BRelation<Integer,Integer> chat_tmp = machine.get_chat();
			BRelation<Integer,Integer> active_tmp = machine.get_active();
			BRelation<Integer,Integer> toread_tmp = machine.get_toread();
			BRelation<Integer,Integer> inactive_tmp = machine.get_inactive();

			machine.set_muted(muted_tmp.difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))));
			machine.set_chatcontent(null); // Set Comprehension: feature not supported by EventB2Java
			machine.set_chat(chat_tmp.difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))));
			machine.set_active(active_tmp.difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))));
			machine.set_toread(toread_tmp.difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))));
			machine.set_inactive(inactive_tmp.difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(u1,u2))));

			System.out.println("delete_chat_session executed u1: " + u1 + " u2: " + u2 + " ");
		}
	}

}
