package pl.kriminetskiy.govoroon3.govoroon_sequential;

import pl.kriminetskiy.govoroon3.eventb_prelude.*;
import pl.kriminetskiy.govoroon3.Util.Utilities;

public class broadcast{
	/*@ spec_public */ private machine3 machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public broadcast(machine3 m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_user().has(u1) && uu.isSubset(machine.get_user()) && !uu.equals(BSet.EMPTY) && machine.CONTENT.difference(machine.get_content()).has(c) && machine.get_chatcontent().domain().has(u1) &&  (\forall Integer u;((uu.has(u) && machine.get_user().has(u) && machine.get_active().domain().has(u)) ==> (!machine.get_active().apply(u).equals(u1)))) &&  (\forall Integer u;((uu.has(u) && machine.get_user().has(u) && machine.get_active().domain().has(u1)) ==> (!machine.get_active().apply(u1).equals(u))))); */
	public /*@ pure */ boolean guard_broadcast( Integer c, Integer u1, BSet<Integer> uu) {
		return (machine.get_user().has(u1) && uu.isSubset(machine.get_user()) && !uu.equals(BSet.EMPTY) && machine.CONTENT.difference(machine.get_content()).has(c) && machine.get_chatcontent().domain().has(u1) && true && true);
	}

	/*@ public normal_behavior
		requires guard_broadcast(c,u1,uu);
		assignable machine.content, machine.chat, machine.chatcontent, machine.toread, machine.inactive, machine.owner, machine.contentsize, machine.contentseq;
		ensures guard_broadcast(c,u1,uu) &&  machine.get_content().equals(\old((machine.get_content().union(new BSet<Integer>(c))))) &&  machine.get_chat().equals(\old((machine.get_chat().union(BRelation.cross(uu,new BSet<Integer>(u1)).union(BRelation.cross(new BSet<Integer>(u1),uu)))))) &&  machine.get_chatcontent().equals(\old((machine.get_chatcontent().override(new BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(new Pair<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(u1,(machine.get_chatcontent().apply(u1).override((new Best<Integer>(new JMLObjectSet {Integer u | (\exists Integer e; (uu.has(u)); e.equals(new Pair<Pair<Integer,BSet<Integer>>,ERROR>(new Pair<Integer,ERROR>(u1,u),new BSet<Integer>(c))))}).union(new Best<Integer>(new JMLObjectSet {Integer u | (\exists Integer e; (uu.has(u)); e.equals(new Pair<Pair<Integer,BSet<Integer>>,ERROR>(new Pair<Integer,ERROR>(u,u1),new BSet<Integer>(c))))}))))))))))) &&  machine.get_toread().equals(\old((machine.get_toread().union(BRelation.cross(uu,new BSet<Integer>(u1)))))) &&  machine.get_inactive().equals(\old((machine.get_inactive().union(BRelation.cross(new BSet<Integer>(u1),uu))))) &&  machine.get_owner().equals(\old((machine.get_owner().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(c,u1)))))) &&  machine.get_contentsize() == \old(new Integer(machine.get_contentsize() + 1)) &&  machine.get_contentseq().equals(\old((machine.get_contentseq().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(new Integer(machine.get_contentsize() + 1),c)))))); 
	 also
		requires !guard_broadcast(c,u1,uu);
		assignable \nothing;
		ensures true; */
	public void run_broadcast( Integer c, Integer u1, BSet<Integer> uu){
		if(guard_broadcast(c,u1,uu)) {
			BSet<Integer> content_tmp = machine.get_content();
			BRelation<Integer,Integer> chat_tmp = machine.get_chat();
			BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> chatcontent_tmp = machine.get_chatcontent();
			BRelation<Integer,Integer> toread_tmp = machine.get_toread();
			BRelation<Integer,Integer> inactive_tmp = machine.get_inactive();
			BRelation<Integer,Integer> owner_tmp = machine.get_owner();
			Integer contentsize_tmp = machine.get_contentsize();
			BRelation<Integer,Integer> contentseq_tmp = machine.get_contentseq();

			machine.set_content((content_tmp.union(new BSet<Integer>(c))));
			machine.set_chat((chat_tmp.union(BRelation.cross(uu,new BSet<Integer>(u1)).union(BRelation.cross(new BSet<Integer>(u1),uu)))));
			machine.set_chatcontent(null); // Set Comprehension: feature not supported by EventB2Java
			machine.set_toread((toread_tmp.union(BRelation.cross(uu,new BSet<Integer>(u1)))));
			machine.set_inactive((inactive_tmp.union(BRelation.cross(new BSet<Integer>(u1),uu))));
			machine.set_owner((owner_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(c,u1)))));
			machine.set_contentsize(new Integer(contentsize_tmp + 1));
			machine.set_contentseq((contentseq_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(new Integer(contentsize_tmp + 1),c)))));

			System.out.println("broadcast executed c: " + c + " u1: " + u1 + " uu: " + uu + " ");
		}
	}

}
