package pl.kriminetskiy.govoroon3.govoroon_sequential;

import pl.kriminetskiy.govoroon3.eventb_prelude.BRelation;
import pl.kriminetskiy.govoroon3.eventb_prelude.BSet;
import pl.kriminetskiy.govoroon3.eventb_prelude.Pair;

public class add_user {
	/* @ spec_public */ private machine3 machine; // reference to the machine

	/*
	 * @ public normal_behavior requires true; assignable \everything; ensures
	 * this.machine == m;
	 */
	public add_user(machine3 m) {
		this.machine = m;
	}

	/*
	 * @ public normal_behavior requires true; assignable \nothing; ensures
	 * \result <==> machine.USER.difference(machine.get_user()).has(u);
	 */
	public /* @ pure */ boolean guard_add_user(Integer u) {
		return machine.USER.difference(machine.get_user()).has(u);
	}

	/*
	 * @ public normal_behavior requires guard_add_user(u); assignable
	 * machine.chatcontent, machine.user; ensures guard_add_user(u) &&
	 * machine.get_chatcontent().equals(\old((machine.get_chatcontent().override
	 * (new BRelation<Integer,ERROR>(new
	 * Pair<Integer,ERROR>(u,BRelation.EMPTY)))))) &&
	 * machine.get_user().equals(\old((machine.get_user().union(new
	 * BSet<Integer>(u))))); also requires !guard_add_user(u); assignable
	 * \nothing; ensures true;
	 */
	public void run_add_user(Integer u) {
		if (guard_add_user(u)) {
			BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>> chatcontent_tmp = machine
					.get_chatcontent();
			BSet<Integer> user_tmp = machine.get_user();

			machine.set_chatcontent(chatcontent_tmp.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u, BRelation.EMPTY))));
			machine.set_user((user_tmp.union(new BSet<Integer>(u))));

			System.out.println("add_user executed u: " + u + " ");
		}
	}

}
