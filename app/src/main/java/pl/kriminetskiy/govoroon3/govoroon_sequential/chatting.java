package pl.kriminetskiy.govoroon3.govoroon_sequential;

import pl.kriminetskiy.govoroon3.eventb_prelude.*;
import pl.kriminetskiy.govoroon3.Util.Utilities;

public class chatting{
	/*@ spec_public */ private machine3 machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public chatting(machine3 m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_user().has(u1) && machine.get_user().has(u2) && machine.CONTENT.difference(machine.get_content()).has(c) && machine.get_active().has(new Pair<Integer,Integer>(u1,u2)) && !machine.get_muted().has(new Pair<Integer,Integer>(u1,u2)) && !machine.get_muted().has(new Pair<Integer,Integer>(u2,u1)) && machine.get_chatcontent().domain().has(u1) && machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().apply(u1).domain().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().apply(u1).domain().has(new Pair<Integer,Integer>(u2,u1)) &&  new_rc.domain().isSubset(machine.get_user()) && new_rc.range().isSubset(((machine.get_content()).pow())) && new_rc.isaFunction() && BRelation.cross(machine.get_user(),((machine.get_content()).pow())).has(new_rc) && ((new_rc.domain().has(u1) && machine.get_reading_content().domain().has(u1) && machine.get_chatcontent().domain().has(u2) && machine.get_chatcontent().apply(u2).domain().has(new Pair<Integer,Integer>(u1,u2))) ==> (new_rc.apply(u1).equals((machine.get_chatcontent().apply(u2).apply(new Pair<Integer,Integer>(u1,u2)).union(machine.get_chatcontent().apply(u1).apply(new Pair<Integer,Integer>(u1,u2)).union(new BSet<Integer>(c))))))) || ((new_rc.domain().has(u1) && machine.get_reading_content().domain().has(u1) && !(machine.get_chatcontent().domain().has(u2) || machine.get_chatcontent().apply(u2).domain().has(new Pair<Integer,Integer>(u1,u2)))) ==> (new_rc.apply(u1).equals((machine.get_chatcontent().apply(u1).apply(new Pair<Integer,Integer>(u1,u2)).union(new BSet<Integer>(c))))))); */
	public /*@ pure */ boolean guard_chatting( Integer c, Integer u1, Integer u2, BRelation<Integer,BSet<Integer>> new_rc) {
		return (machine.get_user().has(u1) && machine.get_user().has(u2) && machine.CONTENT.difference(machine.get_content()).has(c) && machine.get_active().has(new Pair<Integer,Integer>(u1,u2)) && !machine.get_muted().has(new Pair<Integer,Integer>(u1,u2)) && !machine.get_muted().has(new Pair<Integer,Integer>(u2,u1)) && machine.get_chatcontent().domain().has(u1) && machine.get_chat().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().apply(u1).domain().has(new Pair<Integer,Integer>(u1,u2)) && machine.get_chatcontent().apply(u1).domain().has(new Pair<Integer,Integer>(u2,u1)) &&  new_rc.domain().isSubset(machine.get_user()) && new_rc.range().isSubset(((machine.get_content()).pow())) && new_rc.isaFunction() && BRelation.cross(machine.get_user(),((machine.get_content()).pow())).has(new_rc) && BOOL.implication(new_rc.domain().has(u1) && machine.get_reading_content().domain().has(u1) && machine.get_chatcontent().domain().has(u2) && machine.get_chatcontent().apply(u2).domain().has(new Pair<Integer,Integer>(u1,u2)),new_rc.apply(u1).equals((machine.get_chatcontent().apply(u2).apply(new Pair<Integer,Integer>(u1,u2)).union(machine.get_chatcontent().apply(u1).apply(new Pair<Integer,Integer>(u1,u2)).union(new BSet<Integer>(c)))))) || BOOL.implication(new_rc.domain().has(u1) && machine.get_reading_content().domain().has(u1) && !(machine.get_chatcontent().domain().has(u2) || machine.get_chatcontent().apply(u2).domain().has(new Pair<Integer,Integer>(u1,u2))),new_rc.apply(u1).equals((machine.get_chatcontent().apply(u1).apply(new Pair<Integer,Integer>(u1,u2)).union(new BSet<Integer>(c))))));
	}

	/*@ public normal_behavior
		requires guard_chatting(c,u1,u2,new_rc);
		assignable machine.content, machine.chatcontent, machine.toread, machine.owner, machine.contentsize, machine.contentseq, machine.reading_content;
		ensures guard_chatting(c,u1,u2,new_rc) &&  machine.get_content().equals(\old((machine.get_content().union(new BSet<Integer>(c))))) &&  machine.get_chatcontent().equals(\old((machine.get_chatcontent().override(new BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(new Pair<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(u1,(machine.get_chatcontent().apply(u1).override(new BRelation<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Integer,Integer>(u1,u2),(machine.get_chatcontent().apply(u1).apply(new Pair<Integer,Integer>(u1,u2)).union(new BSet<Integer>(c)))),new Pair<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Integer,Integer>(u2,u1),(machine.get_chatcontent().apply(u1).apply(new Pair<Integer,Integer>(u2,u1)).union(new BSet<Integer>(c))))))))))))) &&  machine.get_toread().equals(\old((machine.get_toread().union(machine.get_inactive().restrictDomainTo(new BSet<Integer>(u2)).restrictRangeTo(new BSet<Integer>(u1)))))) &&  machine.get_owner().equals(\old((machine.get_owner().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(c,u1)))))) &&  machine.get_contentsize() == \old(new Integer(machine.get_contentsize() + 1)) &&  machine.get_contentseq().equals(\old((machine.get_contentseq().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(new Integer(machine.get_contentsize() + 1),c)))))) &&  machine.get_reading_content().equals(\old((machine.get_reading_content().override(new_rc)))); 
	 also
		requires !guard_chatting(c,u1,u2,new_rc);
		assignable \nothing;
		ensures true; */
	public void run_chatting( Integer c, Integer u1, Integer u2, BRelation<Integer,BSet<Integer>> new_rc){
		if(guard_chatting(c,u1,u2,new_rc)) {
			BSet<Integer> content_tmp = machine.get_content();
			BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>> chatcontent_tmp = machine.get_chatcontent();
			BRelation<Integer,Integer> toread_tmp = machine.get_toread();
			BRelation<Integer,Integer> owner_tmp = machine.get_owner();
			Integer contentsize_tmp = machine.get_contentsize();
			BRelation<Integer,Integer> contentseq_tmp = machine.get_contentseq();
			BRelation<Integer,BSet<Integer>> reading_content_tmp = machine.get_reading_content();

			machine.set_content((content_tmp.union(new BSet<Integer>(c))));
			machine.set_chatcontent((chatcontent_tmp.override(new BRelation<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(new Pair<Integer,BRelation<Pair<Integer,Integer>,BSet<Integer>>>(u1,(chatcontent_tmp.apply(u1).override(new BRelation<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Integer,Integer>(u1,u2),(chatcontent_tmp.apply(u1).apply(new Pair<Integer,Integer>(u1,u2)).union(new BSet<Integer>(c)))),new Pair<Pair<Integer,Integer>,BSet<Integer>>(new Pair<Integer,Integer>(u2,u1),(chatcontent_tmp.apply(u1).apply(new Pair<Integer,Integer>(u2,u1)).union(new BSet<Integer>(c))))))))))));
			machine.set_toread((toread_tmp.union(machine.get_inactive().restrictDomainTo(new BSet<Integer>(u2)).restrictRangeTo(new BSet<Integer>(u1)))));
			machine.set_owner((owner_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(c,u1)))));
			machine.set_contentsize(new Integer(contentsize_tmp + 1));
			machine.set_contentseq((contentseq_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(new Integer(contentsize_tmp + 1),c)))));
			machine.set_reading_content((reading_content_tmp.override(new_rc)));

			System.out.println("chatting executed c: " + c + " u1: " + u1 + " u2: " + u2 + " new_rc: " + new_rc + " ");
		}
	}

}
