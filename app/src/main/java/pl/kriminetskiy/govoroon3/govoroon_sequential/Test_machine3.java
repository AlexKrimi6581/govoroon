package pl.kriminetskiy.govoroon3.govoroon_sequential;

import java.util.Random;

import pl.kriminetskiy.govoroon3.Util.Utilities;
import pl.kriminetskiy.govoroon3.eventb_prelude.BOOL;
import pl.kriminetskiy.govoroon3.eventb_prelude.BRelation;
import pl.kriminetskiy.govoroon3.eventb_prelude.BSet;
import pl.kriminetskiy.govoroon3.eventb_prelude.Enumerated;
import pl.kriminetskiy.govoroon3.eventb_prelude.Pair;

public class Test_machine3 {

	static Random rnd = new Random();
	static Integer max_size_BSet = 10;
	Integer min_integer = Utilities.min_integer;
	Integer max_integer = Utilities.max_integer;

	public Integer GenerateRandomInteger() {
		BSet<Integer> S = new BSet<Integer>(new Enumerated(min_integer, max_integer));
		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return (Integer) S.toArray()[rnd.nextInt(S.size())];
	}

	public boolean GenerateRandomBoolean() {
		boolean res = (Boolean) BOOL.instance.toArray()[rnd.nextInt(2)];

		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return res;
	}

	public BSet<Integer> GenerateRandomIntegerBSet() {
		int size = rnd.nextInt(max_size_BSet);
		BSet<Integer> S = new BSet<Integer>();
		while (S.size() != size) {
			S.add(GenerateRandomInteger());
		}

		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return S;
	}

	public BSet<Boolean> GenerateRandomBooleanBSet() {
		int size = rnd.nextInt(2);
		BSet<Boolean> res = new BSet<Boolean>();
		if (size == 0) {
			res = new BSet<Boolean>(GenerateRandomBoolean());
		} else {
			res = new BSet<Boolean>(true, false);
		}

		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return res;
	}

	public BRelation<Integer, Integer> GenerateRandomBRelation() {
		BRelation<Integer, Integer> res = new BRelation<Integer, Integer>();
		int size = rnd.nextInt(max_size_BSet);
		while (res.size() != size) {
			res.add(new Pair<Integer, Integer>(GenerateRandomInteger(), GenerateRandomInteger()));
		}
		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return res;
	}

	public static void perform_chatting(machine3 machine, Integer c, Integer u11, Integer u22) {

		Pair<Integer, Integer> chat1 = new Pair<Integer, Integer>(u11, u22);
		BSet<Integer> old_chat_cotent = machine.get_chatcontent().apply(u11).apply(chat1);
		BSet<Integer> old_chat_cotent2 = machine.get_chatcontent().apply(u22).apply(chat1);
		BSet<Integer> new_cc = new BSet<Integer>();

		System.out.println(old_chat_cotent);
		System.out.println(old_chat_cotent2);

		if (!(old_chat_cotent == null) && !(old_chat_cotent.isEmpty())) {
			new_cc = old_chat_cotent;

			if (!(old_chat_cotent2 == null) && !(old_chat_cotent2.isEmpty())) {
				new_cc = new_cc.union(old_chat_cotent2);
			}
		}
		if (new_cc.isEmpty()) {
			if (!(old_chat_cotent2 == null) && !(old_chat_cotent2.isEmpty())) {
				new_cc = old_chat_cotent2;
			}
		}

		if (!new_cc.isEmpty()) {
			new_cc = new_cc.union(new BSet<Integer>(c));
		} else {
			new_cc = new BSet<Integer>(c);
		}

		BRelation<Integer, BSet<Integer>> new_rc = new BRelation<Integer, BSet<Integer>>();

		new_rc.add(u11, new_cc);
		if (machine.get_active().has(new Pair<Integer, Integer>(u22, u11))) {
			new_rc.add(u22, new_cc);
		}

		if (machine.evt_chatting.guard_chatting(c, u11, u22, new_rc)
				&& machine.get_active().has(new Pair<Integer, Integer>(u11, u22)))
			machine.evt_chatting.run_chatting(c, u11, u22, new_rc);

		new_rc.remove(u11, new_cc);
		if (machine.get_active().has(new Pair<Integer, Integer>(u22, u11))) {
			new_rc.remove(u22, new_cc);
		}
	}

	public static void perform_delete_chat_session(machine3 machine, Integer u11, Integer u22) {

		BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>> chatcontent_tmp2 = machine
				.get_chatcontent();
		System.out.println(machine.get_chatcontent().apply(u11).domain());

		for (Integer u : machine.get_user()) {
			if (machine.get_chatcontent().apply(u).domain().has(new Pair<Integer, Integer>(u11, u22))) {
				machine.set_chatcontent(machine.get_chatcontent()
						.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
								new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u,
										machine.get_chatcontent().apply(u)
												.domainSubtraction(new BSet<Pair<Integer, Integer>>(
														new Pair<Integer, Integer>(u11, u22)))))));
			}
		}

		BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>> chatcontent_tmp = machine
				.get_chatcontent();

		machine.set_chatcontent(chatcontent_tmp2);

		if (machine.evt_delete_chat_session.guard_delete_chat_session(u11, u22)) {
			machine.evt_delete_chat_session.run_delete_chat_session(u11, u22);
			machine.set_chatcontent(chatcontent_tmp);
		}
	}

	public static void perform_create_chat_session(machine3 machine, Integer u11, Integer u22) {

		if (machine.evt_create_chat_session.guard_create_chat_session(u11, u22)) {
			machine.evt_create_chat_session.run_create_chat_session(u11, u22);

			if (!machine.get_chatcontent().apply(u11).domain().contains(new Pair<Integer, Integer>(u11, u22))) {
				machine.set_chatcontent((machine.get_chatcontent()
						.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
								new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11, (machine
										.get_chatcontent().apply(u11)
										.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
												new Pair<Pair<Integer, Integer>, BSet<Integer>>(
														new Pair<Integer, Integer>(u11, u22), BRelation.EMPTY)))))))));
			}

			if (!machine.get_chatcontent().apply(u11).domain().has(new Pair<Integer, Integer>(u22, u11))) {
				machine.set_chatcontent((machine.get_chatcontent()
						.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
								new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11, (machine
										.get_chatcontent().apply(u11)
										.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
												new Pair<Pair<Integer, Integer>, BSet<Integer>>(
														new Pair<Integer, Integer>(u22, u11), BRelation.EMPTY)))))))));
			}
		}
	}

	public static void perform_broadcast(machine3 machine, Integer c, Integer u11, BSet<Integer> uuu) {

		for (Integer u : uuu) {
			if (machine.get_chatcontent().apply(u11).domain().has(new Pair<Integer, Integer>(u11, u))) {
				machine.set_chatcontent((machine.get_chatcontent()
						.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
								new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11,
										(machine.get_chatcontent().apply(u11)
												.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
														new Pair<Pair<Integer, Integer>, BSet<Integer>>(
																new Pair<Integer, Integer>(u11, u), (machine
																.get_chatcontent().apply(u11)
																.apply(new Pair<Integer, Integer>(u11, u))
																.union(new BSet<Integer>(c))))))))))));
			} else {
				machine.set_chatcontent((machine.get_chatcontent()
						.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
								new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11,
										(machine.get_chatcontent().apply(u11)
												.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
														new Pair<Pair<Integer, Integer>, BSet<Integer>>(
																new Pair<Integer, Integer>(u11, u),
																new BSet<Integer>(c))))))))));
			}
			if (machine.get_chatcontent().apply(u11).domain().has(new Pair<Integer, Integer>(u, u11))) {
				machine.set_chatcontent((machine.get_chatcontent()
						.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
								new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11,
										(machine.get_chatcontent().apply(u11)
												.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
														new Pair<Pair<Integer, Integer>, BSet<Integer>>(
																new Pair<Integer, Integer>(u, u11), (machine
																.get_chatcontent().apply(u11)
																.apply(new Pair<Integer, Integer>(u, u11))
																.union(new BSet<Integer>(c))))))))))));
			} else {
				machine.set_chatcontent((machine.get_chatcontent()
						.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
								new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11,
										(machine.get_chatcontent().apply(u11)
												.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
														new Pair<Pair<Integer, Integer>, BSet<Integer>>(
																new Pair<Integer, Integer>(u, u11),
																new BSet<Integer>(c))))))))));
			}
		}
		BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>> chatcontent_tmp = machine
				.get_chatcontent();

		if (machine.evt_broadcast.guard_broadcast(c, u11, uuu)) {
			machine.evt_broadcast.run_broadcast(c, u11, uuu);
			machine.set_chatcontent(chatcontent_tmp);
		}
	}

	public static void perform_forward(machine3 machine, Integer c, Integer u11, BSet<Integer> uuu) {

		for (Integer u : uuu) {
			machine.set_chatcontent((machine.get_chatcontent()
					.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
							new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11,
									(machine.get_chatcontent().apply(u11)
											.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
													new Pair<Pair<Integer, Integer>, BSet<Integer>>(
															new Pair<Integer, Integer>(u11,
																	u),
															(machine.get_chatcontent().apply(u11)
																	.apply(new Pair<Integer, Integer>(u11, u))
																	.union(new BSet<Integer>(c))))))))))));
			machine.set_chatcontent((machine.get_chatcontent()
					.override(new BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(
							new Pair<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>>(u11,
									(machine.get_chatcontent().apply(u11)
											.override(new BRelation<Pair<Integer, Integer>, BSet<Integer>>(
													new Pair<Pair<Integer, Integer>, BSet<Integer>>(
															new Pair<Integer, Integer>(u,
																	u11),
															(machine.get_chatcontent().apply(u11)
																	.apply(new Pair<Integer, Integer>(u, u11))
																	.union(new BSet<Integer>(c))))))))))));
		}
		BRelation<Integer, BRelation<Pair<Integer, Integer>, BSet<Integer>>> chatcontent_tmp = machine
				.get_chatcontent();

		if (machine.evt_forward.guard_forward(c, u11, uuu)) {
			machine.evt_forward.run_forward(c, u11, uuu);
			machine.set_chatcontent(chatcontent_tmp);
		}
	}

	public static void perform_delete_content(machine3 machine, Integer c, Integer u11, Integer u22) {
		if (machine.evt_delete_content.guard_delete_content(c, u11, u22)) {
			machine.evt_delete_content.run_delete_content(c, u11, u22);
			BRelation<Integer, BSet<Integer>> new_rc = new BRelation<Integer, BSet<Integer>>();
			System.out.println(machine.get_reading_content().apply(u11));
			//new_rc.add(new Pair <Integer, BSet<Integer>>(u11, machine.get_reading_content().apply(u11).difference(new BSet<Integer>(c))));
			//machine.set_reading_content(machine.get_reading_content().override(new_rc));
		}
	}

	public static void perform_remove_content(machine3 machine, Integer c, Integer u11, Integer u22) {
		if (machine.evt_remove_content.guard_remove_content(c, u11, u22)) {
			machine.evt_remove_content.run_remove_content(c, u11, u22);
			BRelation<Integer, BSet<Integer>> new_rc = new BRelation<Integer, BSet<Integer>>();
			//new_rc.add(new Pair <Integer, BSet<Integer>>(u11, machine.get_reading_content().apply(u11).difference(new BSet<Integer>(c))));
			//machine.set_reading_content(machine.get_reading_content().override(new_rc));
		}
	}
}