package pl.kriminetskiy.govoroon3.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

import pl.kriminetskiy.govoroon3.GovoroonApp;
import pl.kriminetskiy.govoroon3.LoginActivity;
import pl.kriminetskiy.govoroon3.R;
import pl.kriminetskiy.govoroon3.govoroon_sequential.Test_machine3;
import pl.kriminetskiy.govoroon3.govoroon_sequential.add_user;
import pl.kriminetskiy.govoroon3.govoroon_sequential.create_chat_session;
import pl.kriminetskiy.govoroon3.models.ChatItem;
import pl.kriminetskiy.govoroon3.models.User;

import static android.content.ContentValues.TAG;

/**
 * Created by user on 06.11.17.
 */

public class AddChatDialogFragment extends DialogFragment {

    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.dialog_add_chat, null);
        builder.setView(v)
                .setMessage(R.string.add_chat)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        final DatabaseReference database = FirebaseDatabase.getInstance().getReference();

                        final String username = ((EditText) v.findViewById(R.id.username)).getText().toString();

                        final String user1Id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        database.child(GovoroonApp.USERS_STR).child(user1Id).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot1) {
                                final User user1 = dataSnapshot1.getValue(User.class);
                                database.child(GovoroonApp.USERS_STR).orderByChild("email").equalTo(username).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot2) {
                                        if (dataSnapshot2.getValue() == null) {
                                            Log.d(TAG, "User not found!");
                                            Toast toast = Toast.makeText(mContext, "User not found!", Toast.LENGTH_LONG);
                                            toast.show();

                                        }
                                        else
                                        {
                                            final User user2 = dataSnapshot2.getChildren().iterator().next()
                                                    .getValue(User.class);

                                            Log.d("user1.intId", String.valueOf(user1.intId));
                                            Log.d("user2.intId", String.valueOf(user2.intId));

                                            Test_machine3.perform_create_chat_session(
                                                    ((GovoroonApp) ((Activity) mContext).getApplication())
                                                            .getmMachine(), user1.intId, user2.intId
                                            );
                                            ChatItem ci = new ChatItem(
                                                    user2.uid,
                                                    username,
                                                    false,
                                                    false,
                                                    user1.intId,
                                                    user2.intId,
                                                    false);
                                            database.child(GovoroonApp.CHATS_STR).child(user1.uid).child(user2.uid).setValue(ci);

                                            create_chat_session ccs = new create_chat_session(
                                                    ((GovoroonApp) ((Activity) mContext).getApplication())
                                                            .getmMachine()
                                            );
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        return builder.create();
    }
}
