package pl.kriminetskiy.govoroon3.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import pl.kriminetskiy.govoroon3.R;
import pl.kriminetskiy.govoroon3.ForwardActivity;

/**
 * Created by user on 09.11.17.
 */

public class ForwardDeleteDialogFragment extends DialogFragment implements AdapterView.OnItemClickListener {
    private String mSenderId;
    private String mRecipientId;
    private String mMessageId;
    private DatabaseReference mDatabase;
    private String mMessageText;
    private String mSenderEmail;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mSenderId = getArguments().getString("senderId");
        mSenderEmail = getArguments().getString("senderEmail");
        mRecipientId = getArguments().getString("recipientId");
        mMessageId = getArguments().getString("messageId");
        mMessageText = getArguments().getString("messageText");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.dialog_forward_delete, null);
        builder.setView(v);

        ListView lv = (ListView) v.findViewById(R.id.message_actions);
        lv.setOnItemClickListener(this);

        return builder.create();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) { // forward
            forwardMessage();
        }
        else if (position == 1) { // delete
            deleteMessage();
        }
    }

    private void forwardMessage() {
        Intent intent = new Intent(getActivity(), ForwardActivity.class);
        intent.putExtra("senderId", mSenderId);
        intent.putExtra("senderEmail", mSenderEmail);
        intent.putExtra("messageText", mMessageText);
        startActivity(intent);
        dismiss();
    }

    private void deleteMessage() {
        mDatabase.child("messages").child(mSenderId).child(mRecipientId).child(mMessageId).removeValue();
        dismiss();
    }
}
