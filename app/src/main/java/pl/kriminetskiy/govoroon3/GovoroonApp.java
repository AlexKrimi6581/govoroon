package pl.kriminetskiy.govoroon3;

import android.app.Application;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import pl.kriminetskiy.govoroon3.govoroon_sequential.Test_machine3;
import pl.kriminetskiy.govoroon3.govoroon_sequential.add_user;
import pl.kriminetskiy.govoroon3.govoroon_sequential.machine3;
import pl.kriminetskiy.govoroon3.models.ChatItem;
import pl.kriminetskiy.govoroon3.models.User;

public class GovoroonApp extends Application {
    private static final String TAG = "Application";
    public static final String USERS_STR = "1_users";
    public static final String CHATS_STR = "2_chats";
    private DatabaseReference mDatabase;
    public machine3 getmMachine() {
        return mMachine;
    }

    public void setmMachine(machine3 mMachine) {
        this.mMachine = mMachine;
    }

    private machine3 mMachine;

    @Override
    public void onCreate() {
        super.onCreate();
        mMachine = new machine3();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.orderByKey().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> ds = dataSnapshot.child(USERS_STR).getChildren();
                for (DataSnapshot user: ds) {
                    User u = user.getValue(User.class);
                    add_user au = new add_user(mMachine);
                    if (au.guard_add_user(u.intId)) {
                        au.run_add_user(u.intId);
                    }
                }

                ds = dataSnapshot.child(CHATS_STR).getChildren();
                for (DataSnapshot c1: ds) {
                    for (DataSnapshot c2: c1.getChildren())
                    {
                        ChatItem ci = c2.getValue(ChatItem.class);
                        Test_machine3.perform_create_chat_session(mMachine, ci.getIntId1(), ci.getIntId2());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child(GovoroonApp.CHATS_STR).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                ChatItem ci = dataSnapshot.getChildren().iterator().next().getValue(ChatItem.class);
                Test_machine3.perform_delete_chat_session(mMachine, ci.getIntId1(), ci.getIntId2());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
